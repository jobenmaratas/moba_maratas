﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody rb;
    public float speed = 20f;
    public float lifetime = 2.0f;
    public float damage;

    // Use this for initialization
    void Start()
    {
        Invoke("destroyBullet", lifetime);
        rb.velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }


    void destroyBall()
    {
        Destroy(gameObject);
    }
}
