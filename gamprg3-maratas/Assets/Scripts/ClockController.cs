﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockController : MonoBehaviour
{
    [SerializeField] Light sunlight;
    public Text Clock;

    public float seconds;
    public float minutes;

    bool day = true;

    void Start()
    {
        InvokeRepeating("changeLight", 300f, 300f);
    }

    void Update()
    {
        seconds += Time.deltaTime;

        if (seconds >= 60)
        {
            seconds = 0;
            minutes++;
            //changeLight();
        }

        Clock.text = minutes + ":" + seconds.ToString("00");
    }

    void changeLight()
    {
        day = !day;


        if (day)
        {
            sunlight.color = Color.white;

            Debug.Log("DAY");
        }

        else
        {
            sunlight.color = Color.gray;
            Debug.Log("NIGHT");
        }
    }
}
