﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // (Reference) Making a moba cam in unity: https://www.youtube.com/watch?v=Nc7h5-pLFqg

    public float ScrollSpeed;

    public float TopBarrier;
    public float BotBarrier;
    public float LeftBarrier;
    public float RightBarrier;

    void Update()
    {
        if (Input.mousePosition.y >= Screen.height * TopBarrier)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * ScrollSpeed, Space.World);
        }

        if (Input.mousePosition.y <= Screen.height * BotBarrier)
        {
            transform.Translate(Vector3.back * Time.deltaTime * ScrollSpeed, Space.World);
        }

        if (Input.mousePosition.x >= Screen.width * RightBarrier)
        {
            transform.Translate(Vector3.right * Time.deltaTime * ScrollSpeed, Space.World);
        }

        if (Input.mousePosition.x <= Screen.width * LeftBarrier)
        {
            transform.Translate(Vector3.left * Time.deltaTime * ScrollSpeed, Space.World);
        }

        // Clamp position of X
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -12f, 210f), transform.position.y, transform.position.z);

        // Clamp position of Y
        //transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -46f, 46f), transform.position.z);

        // Clamp position of Z
        transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Clamp(transform.position.z, -17f, 210f));

    }
}
