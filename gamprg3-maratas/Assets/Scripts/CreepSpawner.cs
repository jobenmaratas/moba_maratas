﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepSpawner : MonoBehaviour
{
    public GameObject normalCreeps;
    public float spawnTime = 10f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnCreep", 10f, 5f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnCreep()
    {
        Instantiate(normalCreeps, transform.position, transform.rotation);
    }
}
