﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDController : MonoBehaviour
{
    public GameObject scorePanel;
    public GameObject quitPanel;

    public void OpenScoreBoard()
    {
        if (scorePanel.activeSelf)
            scorePanel.SetActive(false);
        else scorePanel.SetActive(true);
    }

    public void OpenQuitPanel()
    {
        if (quitPanel.activeSelf)
            quitPanel.SetActive(false);
        else quitPanel.SetActive(true);
    }
}
