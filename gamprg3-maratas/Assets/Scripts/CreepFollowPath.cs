﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepFollowPath : MonoBehaviour
{
    // This was my code when I took GDEVAI.
    public ClockController clockController;
    public Path path;
    public float speed = 4.0f;
    public float mass = 5.0f;
    public bool isLooping = true;

    //Actual speed of the vehicle 
    private float curSpeed;

    private int curPathIndex;
    private float pathLength;
    private Vector3 targetPoint;

    Vector3 velocity;

    // Use this for initialization
    void Start()
    {
        pathLength = path.Length;
        curPathIndex = 1;

        //get the current velocity of the vehicle
        velocity = transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
        if (clockController.seconds < 5)
        {
            speed = 0;
        }
        else
        {
            speed = 4f;
        }
        //print("Current Index: " + curPathIndex);

        //Unify the speed
        curSpeed = speed * Time.deltaTime;

        targetPoint = path.GetPoint(curPathIndex);

        float distance = Vector3.Distance(transform.position, targetPoint);

        //1- If reach the radius within the path then move to next point in the path
        // CODE HERE

        // If it reaches a point, target changes to another point.
        if (isLooping && Vector3.Distance(transform.position, targetPoint) < 1)
        {
            curPathIndex++;
        }

        if (!isLooping)
        {

            if (curPathIndex == (pathLength - 1))
            {
                if (distance < 10)
                {
                    curSpeed *= distance / 10;
                }
            }

            else if (distance < 1)
            {
                curPathIndex++;
            }
        }

        //2- Move the vehicle until the end point is reached in the path
        // CODE HERE

        // After it reaches the end point, vehicle will go back to the starting point and will make a loop.

        if (isLooping)
        {
            if (curPathIndex >= pathLength)
            {
                curPathIndex = 0;
            }
        }

        //3- Calculate the next Velocity towards the path
        velocity = (targetPoint - transform.position).normalized;
        velocity += AvoidObstacles();
        velocity.Normalize();

        //Rotate the vehicle to its target directional vector
        var rot = Quaternion.LookRotation(velocity);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, 5.0f * Time.deltaTime);

        //Move the vehicle towards the target point 
        transform.position += transform.forward * curSpeed;
    }

    //Steering algorithm to steer the vector towards the target
    public Vector3 Steer(Vector3 target, bool bFinalPoint = false)
    {
        //1- Calculate the directional vector from the current position towards the target point
        Vector3 desiredVelocity = (target - transform.position);
        float dist = desiredVelocity.magnitude;

        //2- Normalise the desired Velocity
        desiredVelocity.Normalize();

        //3- Calculate the velocity according to the speed
        //if FinalPoint is true, gradually decrease the speed
        if (bFinalPoint && dist < 10.0f)
            desiredVelocity *= (curSpeed * (dist / 10.0f));
        else
            desiredVelocity *= curSpeed;

        //4- Calculate the steering force Vector
        Vector3 steeringForce = desiredVelocity - velocity;
        Vector3 acceleration = steeringForce / mass;

        return acceleration;
    }

    public Vector3 AvoidObstacles()
    {
        RaycastHit hit;

        Vector3 left = transform.TransformDirection(Vector3.left);

        // Forward
        if (Physics.Raycast(transform.position, transform.forward, out hit, 2.0f))
        {
            return hit.normal * 2;
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * 2.0f, Color.green);
        }

        // Right
        if (Physics.Raycast(transform.position, transform.right, out hit, 2.0f))
        {
            return hit.normal * 2;
        }
        else
        {
            Debug.DrawRay(transform.position, transform.right * 2.0f, Color.green);
        }

        return Vector3.zero;

    }
}
